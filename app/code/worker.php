<?php
/**
 * General Beanstalk Worker
 *
 * Executes tasks from configured Beanstalk server and executes them.
 *
 * @copyright 2017, Salesupply
 * @author Rutger Kirkels <r.kirkels@salesupply.com>
 * @package salesupply/beanstalk_docker
 */

// Check configuration
if (!file_exists(__DIR__ . '/worker.cfg')) {
    echo 'No configuration found, using default configuration.' . PHP_EOL;
    echo 'Server address: 127.0.0.1' . PHP_EOL;
    echo 'Server port: 11300';
    $config = [
        'host' => '127.0.0.1',
        'port' => 11300
    ];
}
else {
    $jsonConfig = file_get_contents(__DIR__ . '/worker.cfg');
    if (!json_decode($jsonConfig)) {
        echo 'worker.cfg doesn\'t contain valid JSON... exiting' . PHP_EOL;
        die;
    }
    else {
        $configuredConfig = json_decode($jsonConfig);
        $config = [
            'host' => $configuredConfig->beanstalk_server_address,
            'port' => $configuredConfig->beanstalk_server_port
        ];
    }
}

// Load the composer autoloader
require __DIR__ . '/vendor/autoload.php';

$logger = new \Salesupply\Queueing\Helpers\Logger();

// Check if Pheanstalk is available
if (!class_exists('Pheanstalk\\Pheanstalk', true)) {
    echo 'Couldn\'t find Pheanstalk... do you run composer install?' . PHP_EOL;
    die;
}

echo 'Worker ' . getmypid() . ' reporting for duty @' . $config['host'] . ':' . $config['port'] . PHP_EOL;
$mail = new \Salesupply\Queueing\Commands\MailCommand();


$queue = new Pheanstalk\Pheanstalk(
    $config['host'] . ":" . $config['port']
);

if (isset($configuredConfig->queues_to_watch) && is_array($configuredConfig->queues_to_watch)) {
    foreach ($configuredConfig->queues_to_watch as $tube) {
        $queue->watch($tube);
    }
    echo 'Watching ' . count($configuredConfig->queues_to_watch) . ' ';
    echo (count($configuredConfig->queues_to_watch > 1) ?  'tubes' : 'tube');
    echo ': ' . implode(', ', $configuredConfig->queues_to_watch) . PHP_EOL;
}

// pick a job and process it
while($job = $queue->reserve()) {
    $received = json_decode($job->getData(), true);

    $classToInit = $received['class'];
    $methodToExecute = $received['method'];
    if (isset($received['data'])) {
        $args = $received['data'];
    }
    else {
        $args = [];
    }

    if (class_exists($classToInit, true)) {
        $class = new $classToInit;
    } else {
        echo 'Class not found: ' . $classToInit . PHP_EOL;
        $msg = 'Class <b>' . $classToInit . '</b> not found... Task not executed and deleted.';
        echo $msg . PHP_EOL;
        $mail->sendMail('Could not execute task', $msg, ['support@salesupply.com']);
        $queue->delete($job);
        continue;
    }

    if (method_exists($class, $methodToExecute)) {
        $timer = new Timer(true);

        $start = new \DateTime();
        $task = new \Salesupply\Queueing\Models\Task();
        $task->setClass($classToInit);
        $task->setMethod($methodToExecute);
        $task->setArguments($args);
        $task->setStart($start);
        $loggedId = $logger->logTask($task);
        if ($loggedId) {
            $task->setId($loggedId);
        }

        try {
            $result = $class->$methodToExecute($args);
        } catch (\Exception $exception) {
            $task->addDetail($exception->getMessage());
        }


        $task->setEnd(new \DateTime());



        $logger->logTask($task);

        echo 'Worker (' . getmypid() . ') - TASK ' . $job->getId() . ' executed in ' . number_format($timer->getElapsedTime(),5,',','') . ' seconds :' . $classToInit . '::' . $methodToExecute . json_encode($args) . PHP_EOL;
        $queue->delete($job);
    }
    else {
        $msg = 'Method <b>' . $classToInit . '::' . $methodToExecute . '</b> not found... Task not executed and deleted.';
        echo $msg . PHP_EOL;
        $mail->sendMail('Could not execute task', $msg, ['support@salesupply.com']);
        $queue->delete($job);
        continue;
    }
}

class Timer {
    protected $start;

    public function __construct($autostart = false) {
        if ($autostart) {
            $this->start = microtime(true);
        }
    }

    public function reset() {
        $this->start = microtime(true);
    }

    public function getElapsedTime() {
        return microtime(true) - $this->start;
    }
}