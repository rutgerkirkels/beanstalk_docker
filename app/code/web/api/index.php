<?php


require __DIR__ . '/../../vendor/autoload.php';

$app = new Slim\App();

$app->get('/managers/{manager}/{action}', function (\Slim\Http\Request $request, $response, $args) {
    $classToInit = 'Salesupply\\Queueing\\Managers\\' . $args['manager'];
    if (!class_exists($classToInit, true)) {
        return $response
                ->write('Manager not found')
                ->withStatus(404);
    }
    else {
        if (!method_exists(new $classToInit, $args['action'])) {
            return $response
                ->write($args['manager'] . ' has no clue what you mean...')
                ->withStatus(404);
        }
        else {
            $queue = new \Pheanstalk\Pheanstalk('core', 11300);
            $job = [
                'class' => $classToInit,
                'method' => $args['action'],
                'data' => $request->getQueryParams()
            ];
            $taskId = $queue->useTube('management')->put(json_encode($job));
            // Send the job to Beanstalk, using the 'management' tube.
            if ($taskId) {
                // If the job was accepted, display message indicating success.
                $responseData = new \stdClass();
                $responseData->status = 'success';
                $responseData->data = new stdClass();
                $responseData->data->jobId = $taskId;

                return $response
                        ->write(json_encode($responseData))
                        ->withHeader('Content-type', 'application/json');
            }
            else {
                // If the job was not accepted for any reason, we display a general error message.
                echo 'Something went wrong while scheduling the task' . PHP_EOL;
            }
        }
    }
    return $response->write("Hello, " . $args['name']);
});

$app->get('/monitor/workers', function ($request, $response, $args) {
    $api = new \Supervisor\Api('core', 9001);
    $processes = $api->getAllProcessInfo();
    $workers = [];
    foreach ($processes as $process) {
        if ($process['group'] == 'worker') {
            $worker = new stdClass();
            $worker->name = $process['name'];
            $worker->state = $process['statename'];
            $workers[] = $worker;
        }
    }

    return $response
        ->write(json_encode($workers))
        ->withHeader('Content-type', 'application/json')
        ->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/monitor/queues', function ($request, $response, $args) {
    $queue = new \Pheanstalk\Pheanstalk(
        'core:11300'
    );
    $tubes = [];
//    var_dump($queue->listTubes()t);die;
    foreach($queue->listTubes() as $tube) {
        $obj = new stdClass();
        $obj->name = $tube;
        $tubeData = $queue->statsTube($tube);
//        var_dump('<pre>', $tubeData);die;
        $obj->current_jobs_urgent = intval($tubeData->__get('current-jobs-urgent'));
        $obj->current_jobs_ready = intval($tubeData->__get('current-jobs-ready'));
        $obj->current_jobs_reserved= intval($tubeData->__get('current-jobs-reserved'));
        $obj->current_jobs_delayed = intval($tubeData->__get('current-jobs-delayed'));
        $obj->current_jobs_buried = intval($tubeData->__get('current-jobs-buried'));
        $tubes[] = $obj;
    }


    return $response
        ->write(json_encode($tubes))
        ->withHeader('Content-type', 'application/json')
        ->withHeader('Access-Control-Allow-Origin', '*');
});

$app->run();