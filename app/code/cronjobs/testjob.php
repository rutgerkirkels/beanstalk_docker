<?php
define('SLACK_WEBHOOK','https://hooks.slack.com/services/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');

include __DIR__ . '/../vendor/autoload.php';

$slackClient = new \Maknz\Slack\Client(SLACK_WEBHOOK);
$slackClient->from('queue.salesupply.com')->to('@RutgerKirkels')->withIcon(':ghost:')->send(':ok: test');