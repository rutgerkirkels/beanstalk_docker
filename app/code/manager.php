<?php

require __DIR__ . '/vendor/autoload.php';


$config = [
    'host' => '127.0.0.1',
    'port' => 11300
];
$queue = new Pheanstalk\Pheanstalk(
    $config['host'] . ":" . $config['port']
);

$job = [
    'class' => 'SomeClass',
    'method' => 'SomeMethod',
    'data' => [
        'arg1' => 'test',
        'arg2' => 'test2'
        ]
    ];

$queue->useTube('mytube')->put(json_encode($job));

var_dump($queue);