<?php
/**
 * Manager controller
 * Command line tool to start Beanstalk Manager Tasks from defined in the Salesupply\Queueing\Managers namespace.
 * This tool assumes that beanstalkd is installed on the local host with default settings.
 *
 * Usage: php ManagerController.php --manager <ManagerClass> --action <Method_in_Manager_Class>
 *
 * @copyright 2017, Salesupply
 * @author Rutger Kirkels <r.kirkels@salesupply.com>
 */
require __DIR__ . '/vendor/autoload.php';


$config = [
    'host' => '127.0.0.1',
    'port' => 11300
];
$queue = new Pheanstalk\Pheanstalk(
    $config['host'] . ":" . $config['port']
);

// Define the command line arguments
$shortOptions = implode(['m:','a']);
$longOptions = ['manager:','action:'];
$options = getopt($shortOptions, $longOptions);


if (!isset($options['manager'])) {
    // If the manager option is not set, die with error.
    echo 'Manager not set' . PHP_EOL;die;
}
else {

    $classToInit = 'Salesupply\\Queueing\\Managers\\' . $options['manager'];
    if (!class_exists($classToInit, true)) {
        // If the given manager class is not found, die with error.
        echo 'Manager could not be found' . PHP_EOL;die;
    }

    if (!isset($options['action'])) {
        echo 'Action not set' . PHP_EOL;die;
    }
    else {
        if (!method_exists(new $classToInit, $options['action'])) {
            // If the requested method (--action) is not found in the manager class, we return an error.
            echo 'Unknown action: ' . $options['action'] . PHP_EOL;
            echo '------------------------------------------------------------' . PHP_EOL;
            // For the users convience, we list all available method from the manager class, along with their
            // description that we get from their DocBlock.
            echo $options['manager'] . ' has these actions available:'. PHP_EOL;
            $methodsAvailable = get_class_methods ( $classToInit );
            foreach ($methodsAvailable as $methodAvailable) {
                echo '- ' . $methodAvailable;

                // Get the description from the DocBlock
//                $method = new \ReflectionMethod($classToInit, $methodAvailable);
//                $docBlock = $method->getDocComment();
//                if ($docBlock) {
//                    $dbp = new \Sami\Parser\DocBlockParser();
//                    $doc = $dbp->parse($docBlock);
//                    echo trim($doc->getDesc());
//                }
                echo PHP_EOL;
            }
            die;
        }
        else {
            // Generate the beanstalk job.
            $job = [
                'class' => $classToInit,
                'method' => $options['action'],
                'data' => []
            ];

            // Send the job to Beanstalk, using the 'management' tube.
            if ($queue->useTube('management')->put(json_encode($job))) {
                // If the job was accepted, display message indicating success.
                echo 'Manager task created' . PHP_EOL;
            }
            else {
                // If the job was not accepted for any reason, we display a general error message.
                echo 'Something went wrong while scheduling the task' . PHP_EOL;
            }
        }
    }
}
