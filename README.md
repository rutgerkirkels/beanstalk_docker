#Beanstalk Docker
###Authors
* Rutger Kirkels <r.kirkels@salesupply.com>

###Description
Provides a queueing mechanism system, based on Beanstalk. It handles all background tasks.

The system consists of three Docker instances:
* Core: Contains queue and workers.
* Web: Contains NGINX-based REST API
* Fpm: Holds PHP-FPM to facilitate web-requests

###Installation
#####Requirements
You need to have Composer (http://getcomposer.org) installed on your Docker host.

#####Instruction
After you have cloned this repository, you will need to install the dependencies, using Composer:
* cd app/code
* composer install

This installs all libraries, needed to setup the complete functionality for the queue system.

###Directories
* /app/code: Contains PHP-code that is meant to run from the command line:
  * **worker.php**<br/>
  This script looks at the queues and executes the first available task that comes
   avalable for processing. To configure the queue that this worker has to watch, edit
   _worker.cfg_.
   <br/>
  * **ManagerController.php**<br/>
  This is a command line tool

###REST API
The API can be accessed through: [host]/api.